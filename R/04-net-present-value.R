#' @title Net Present Value
#'
#' @import mathjaxr
#'
#' @description
#' `NetPresentValue()` is a function that calculates the net present value.
#'
#' @details
#' The formula used for calculation is:
#'
#' \loadmathjax
#' \mjdeqn{NPV(t, R_t, i, PVB) = PVB - \frac{R_t}{(1 + i)^{t}}}{NPV(t, R_t, i, PVB) = PVB - R_t / ( ( 1 + i )^t )}
#'
#' @param t The time in years.
#' @param R_t The returns or cashflow for each t.
#' @param i The discount rate.
#' @param PVB The present value benefits.
#'
#' @return NPV The net present value (Nettobarwert)
#'
#' @seealso \code{\link[ent]{DiscountFactor}}
#' @seealso https://en.wikipedia.org/wiki/Net_present_value
#' @seealso https://de.wikipedia.org/wiki/Kapitalwert
#' @seealso https://de.wikipedia.org/wiki/Abzinsung_und_Aufzinsung
#' @seealso https://raw.githubusercontent.com/wviechtb/mathjaxr/refs/heads/master/README.md
#'
#' @import ggplot2
#'
#' @export
#' @keywords entrepreneurship accounting
#' @keywords net present value
#'
#' @examples
#' # calculates net present value for
#' # a time frame of 12 years
#' # an investment of 50000
#' # a discount rate of 10% and
#' # a return of 10000
#' NetPresentValue(t = 12, R_t = 50000.00, i = 0.10, PVB = 1000.00)
#' # [1] 8264.463
#'
#' @author
#' Copyright (C) 2021 M.E.Rosner
NetPresentValue <- function(t = 12, R_t = 50000.00, i = 0.10, PVB = 1000.00){
  # time as year
  tt <- seq(1, t, by=1)
  # benefit per year
  # R_t <- 10000
  # interests
  # i <- 0.1
  # vector of present values (PV)
  PVC <- PresentValue(t = tt, R_t = R_t, i = i)
  NPV <- PVB - sum(PVC)
  return(NPV)
}

#' @title TODO: Plot Net Present Value
#'
#' @description
#' `PlotNetPresentValue()` is a function that plots the net present
#' value and benefits.
#'
#' @details
#' The used formula can be found at PresentValue and NetpresentValue
#'
#' @param PVC The present value benefits.
#' @param R_t The returns or cashflow for each t.
#' @param i The discount rate.
#' @param t The time in years.
#'
#' @return NPV The net present value (Nettobarwert)
#'
#' @seealso https://en.wikipedia.org/wiki/Net_present_value
#' @seealso https://de.wikipedia.org/wiki/Kapitalwert
#' @seealso https://de.wikipedia.org/wiki/Abzinsung_und_Aufzinsung
#'
#' @export
#' @keywords entrepreneurship accounting
#' @keywords net present value
#'
#' @examples
#' # calculates present value for
#' # a time frame of two years
#' # a discount rate of 10% and
#' # a return of 10000
#' PresentValue(t = 2, R_t = 10000, i = 0.10)
#' # [1] 8264.463
#'
#' @examples
#' # calculates present value for
#' # present value costs of 1000
#' # yearly return of 52.71
#' # discount rate of 0.01
#' # for a time frame of 48 years
#' PlotNetPresentValue(t = 48, R_t = 52.67, i = 0.01, PVC = 1000.00)
#'
#' @examples
#' # calculates present value for
#' # present value costs of 1000
#' # yearly return of 52.71
#' # discount rate of -0.005
#' # for a time frame of 48 years
#' PlotNetPresentValue(t = 48, R_t = 52.67, i = -0.005, PVC = 1000.00)
#'
#' @author
#' Copyright (C) 2023 M.E.Rosner
PlotNetPresentValue <- function(t = 48, R_t = 52.67, i = -0.005, PVC = 1000.00) {
  requireNamespace("ggplot2", quietly = TRUE)
  
  t <- seq(0, t, by=1)
  PV <- PresentValue(t = t, R_t = R_t, i = i)
  PV[1] <- -(PVC)
  # print(PV)
  #NPV <- sum(PV[2:length(tt)])
  NPV <- sum(PV)

  # require("ggplot2")
  
  #  # time as year
  #  tt <- seq(1, t, by=1)
  #  # benefit per year
  #  # R_t <- 10000
  #  # interests
  #  # i <- 0.1
  #  # vector of present values (PV)
  #  PVB <- PresentValue(R_t, i, tt)
  #  NPV <- sum(PVB) - PVC
  #  print(NPV)
  
  label = paste("PVC", ": ", round(PVC, digits=2), "; ",
		"R_t", ": ", round(R_t, digits=2), "; ",
		"i", ": ", i, "; ",
		"t", ": ", t[length(t)], "\n",
		"NPV", ": ", round(NPV, digits=2), sep="")
  
  # build dataframe
  df <- data.frame(t=t, PV=PV)
  plt <- ggplot2::ggplot(df) +
    ggplot2::geom_line(ggplot2::aes(x=t, y=PV, color="present value")) +
    ggplot2::geom_line(ggplot2::aes(x=t, y=PV[1], color="present value costs")) +
    ggplot2::geom_line(ggplot2::aes(x=t, y=NPV, color="net present value")) +
    ggplot2::labs(
      x = "t - time in years",
      y = "EUR - price, costs, revenue and benefit",
      title = "Net Present Value",
      subtitle = label, # "Source: http://example.org",
      colour = "legend",
      fill = "."
      ) 
  return(plt)
}
