#' @title Discount Factor
#'
#' @import mathjaxr
#'
#' @description
#' `DiscountFactor()` is a function that calculates the discount factor.
#'
#' @details
#' The formula used for calculation is:
#'
#' \loadmathjax
#' \mjdeqn{DF(t, r) = \frac{1}{(1 + r)^{t}}}{DF(t, r) = 1 / ((1 + (r))^(t))}
#' 
#' @param t The time in years
#' @param r The rate of return (Rendite).
#'
#' @return DF The discount factor.
#'
#' @seealso https://en.wikipedia.org/wiki/Discounting
#' @seealso https://de.wikipedia.org/wiki/Abzinsung_und_Aufzinsung
#'
#' @export
#' @keywords entrepreneurship entre accounting 
#' @keywords discount factor
#' @keywords rate of return
#' @keywords cultural agreement
#'
#' @examples
#' # calculate for rate of return of 0.12
#' # a period of 2 years
#' # and 360 days (EUR)
#' DiscountFactor(t = 5, r = 0.12)
#' # [1] 0.5674269
#'
#' @author
#' Copyright (C) 2021 M.E.Rosner
DiscountFactor <- function(t = 5, r = 0.12){
  DF <- 1 / ((1 + (r))^(t))
  return(DF)
}
