#' @title Compound Interest
#'
#' @import mathjaxr
#'
#' @description
#' `CompoundInterest()` is a function that calculates compound interest.
#'
#' @details
#' The formula used for calculation is:
#'
#' \loadmathjax
#' common formula
#' \mjdeqn{ K_n = K_0 * (\frac{100 + p}{100}) ^ n }{ K_n = K_0 * (\frac{100 + p}{100}) ^ n }
#'
#' adapted formulas
#' \mjdeqn{ q = (1 + p) }{ q = (1 + p) }
#' \mjdeqn{ K_n = K_0 * q ^ n }{ K_n = K_0 * q ^ n }
#'
#' @param n The number of years.
#' @param p The annual interest rate (p < 1.0).
#' @param K_0 The initial deposit.
#'
#' @return K_n The compound interest.
#'
#' @seealso LINK for support of mathj
#'
#' https://www.mathjax.org/
#' https://cran.r-project.org/web/packages/mathjaxr/readme/README.html
#' https://stackoverflow.com/questions/14041601/documenting-equations-with-deqn-and-roxygen
#'
#' @export
#' @keywords entrepreneurship entre calculating compound interest
#' 
#' @examples
#' # calculates the compound interest for initial 100 monetary units
#' # using an annual interest rate of 2.5 %
#' # over three years
#' ci_7 <- K_n(K_0 = 100, p = 0.0025, n = 7)
#' ci_7
#' # [1] 101.7632
#'
#' @examples
#' # calculates the compound interest for initial 100 monetary units
#' # using an annual interest rate of 2.5 %
#' # over three years
#' ci_3 <- K_n(K_0 = 100, p = 0.025, n = 3)
#' ci_3
#' # [1] 107.6891
#'
#' #K_n <- function(K_0, p, n) {
#' #annually
#' # q <- (100 + p) / 100
#' # K_n <- K_0 * q ^ n
#' # return(K_n)
#' #}
#'
#' @author
#' Copyright (C) 2023 M.E.Rosner
#'
K_n <- function(K_0 = 100, p = 0.025, n = 3) {
  # adapted to use with p < 1.0
  q <- (1 + p)
  K_n <- K_0 * q ^ n
  return(K_n)
}

