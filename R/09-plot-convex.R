#' @title Plot convex function
#'
#' @import mathjaxr
#'
#' @description
#' `PlotConvex()` is a function that plots a convex function.
#'
#' @details
#' The formula used for calculation is:
#'
#' \loadmathjax
#' \mjdeqn{f(x) = ((1/2)^{x}) + (2*x^{2})}{((1/2)^x) + (2*x^2)}
#' 
#' @param x The vector of numeric x values.
#' @param y The vector of numeric y values / Convex().
#' @param label The curves label.
#'
#' @return plt The graphics object plt.
#'
#' @seealso https://mjo.osborne.economics.utoronto.ca/index.php/tutorial/index/1/cv1/t
#' @seealso https://en.wikipedia.org/wiki/Convex_function
#'
#' @export
#' @keywords entrepreneurship convex
#'
#' @examples
#' x <- seq(-6, 6, 1)
#' y <- Convex(x = x)
#' x
#' # [1] -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6
#' y
#' # [1] 136.00000  82.00000  48.00000  26.00000  12.00000   4.00000   1.00000
#' # [8]   2.50000   8.25000  18.12500  32.06250  50.03125  72.01562
#' plt <- PlotConvex(x = x, y = y, label = "Convex")
#' plt
#'
#' @author
#' Copyright (C) 2025 M.E.Rosner
PlotConvex <- function(x = seq(-6, 6, 1),
                        y = Convex(x = x),
                    label = "Utility") {
  #require("ggplot2")
  df <- data.frame(x = x, y = y)
  #require("ggplot2")
  plt <- ggplot2::ggplot(df) +
      ggplot2::geom_line(ggplot2::aes(x = x, y = y, color = label))
  return(plt)
}
