#!/bin/sh

# remember current path
CUR=$(pwd)

cd tools
R --no-save < build.R

cd ${CUR}

exit 0
